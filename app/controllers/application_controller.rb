class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  before_action :devise_parameter_sanitizer, if: :devise_controller?

  protected

  def devise_parameter_sanitizer
    if resource_class == User
      User::ParameterSanitizer.new(User, :user, params)
    else
      super
    end
  end
end
