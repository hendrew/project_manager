class ProjectsController < ApplicationController
  before_action :set_project, only: %i[edit update destroy]

  def index
    @projects = current_user.projects.order(id: :desc)
  end

  def new
    @project = current_user.projects.build
  end

  def create
    @project = current_user.projects.build(project_params)

    if @project.save
      flash[:success] = 'Projeto cadastrado com sucesso.'
      redirect_to projects_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @project.update(project_params)
      flash[:success] = 'Projeto atualizado com sucesso.'
      redirect_to projects_path
    else
      render :edit
    end
  end

  def destroy
    flash[:success] = 'Projeto excluído com sucesso.' if @project.destroy
    redirect_to projects_path
  end

  private

  def project_params
    params.require(:project).permit(:title)
  end

  def set_project
    @project = current_user.projects.find(params[:id])
  end
end
