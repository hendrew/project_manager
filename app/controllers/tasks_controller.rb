class TasksController < ApplicationController
  before_action :set_project
  before_action :set_task, only: %i[edit update destroy]

  def index
    @tasks = @project.tasks.order(id: :desc)
  end

  def new
    @task = @project.tasks.build
  end

  def create
    @task = @project.tasks.build(task_params)

    if @task.save
      flash[:success] = 'Tarefa cadastrada com sucesso.'
      redirect_to project_tasks_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @task.update(task_params)
      flash[:success] = 'Tarefa atualizada com sucesso.'
      redirect_to project_tasks_path
    else
      render :edit
    end
  end

  def destroy
    flash[:success] = 'Tarefa excluída com sucesso.' if @task.destroy
    redirect_to project_tasks_path
  end

  private

  def task_params
    params.require(:task).permit(:description, :status)
  end

  def set_task
    @task = @project.tasks.find(params[:id])
  end

  def set_project
    @project = current_user.projects.find(params[:project_id])
  end
end
