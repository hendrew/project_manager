class User::ParameterSanitizer < Devise::ParameterSanitizer
  def initialize(*)
    super
    keys = %i[first_name last_name nickname email password password_confirmation]
    permit(:sign_up, keys: keys)
    permit(:account_update, keys: keys)
  end
end
