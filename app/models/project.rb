class Project < ApplicationRecord
  belongs_to :user
  has_many :tasks, dependent: :delete_all

  validates :title, presence: true
end
