class Task < ApplicationRecord
  belongs_to :project

  validates :description, :status, presence: true
end
