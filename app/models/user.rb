class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :rememberable, :validatable, authentication_keys: [:nickname]

  has_many :projects

  validates :first_name, :last_name, :nickname, presence: true
  validates :nickname, uniqueness: true
end
