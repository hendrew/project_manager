FactoryBot.define do
  factory :project do
    title { Faker::Lorem.sentence }
    user { nil }
  end
end
