FactoryBot.define do
  factory :task do
    description { Faker::Lorem.paragraph }
    status { 1 }
    project { nil }
  end
end
