require 'rails_helper'

RSpec.feature 'Profiles', type: :feature do
  let!(:user) { create(:user, password: '123456') }

  before do
    login_as(user, scope: :user)
  end

  context 'user account update' do
    before { visit edit_user_registration_path }

    scenario 'when data is valid' do
      fill_in 'Primeiro nome', with: 'John'
      fill_in 'Último nome', with: 'Doe'
      fill_in 'Nome de usuário', with: 'doe'
      fill_in 'E-mail', with: 'example@example.com'
      fill_in 'Senha', with: '12345678'
      fill_in 'Confirmar senha', with: '12345678'
      fill_in 'Senha atual', with: '123456'

      click_button 'Atualizar'
      expect(page).to have_content 'Sua conta foi atualizada com sucesso.'
    end

    scenario 'when data is valid' do
      fill_in 'Primeiro nome', with: ''
      fill_in 'Último nome', with: ''
      fill_in 'Nome de usuário', with: ''
      fill_in 'E-mail', with: ''
      fill_in 'Senha', with: ''
      fill_in 'Confirmar senha', with: ''
      fill_in 'Senha atual', with: ''

      click_button 'Atualizar'
      expect(page).to have_content 'Não foi possível salvar usuário: 5 erros.'
      expect(page).to have_content 'E-mail não pode ficar em branco'
      expect(page).to have_content 'Primeiro nome não pode ficar em branco'
      expect(page).to have_content 'Último nome não pode ficar em branco'
      expect(page).to have_content 'Nome de usuário não pode ficar em branco'
      expect(page).to have_content 'Senha atual não pode ficar em branco'
    end
  end
end
