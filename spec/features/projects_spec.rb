require 'rails_helper'

RSpec.feature 'Projects', type: :feature do
  let!(:user) { create(:user) }

  before do
    login_as(user, scope: :user)
  end

  context 'project index' do
    let!(:projects) { create_list(:project, 5, user: user) }

    before do
      visit projects_path
    end

    scenario 'must list projects' do
      expect(page).to have_http_status(:ok)

      projects.each do |project|
        expect(page).to have_content project.id
        expect(page).to have_content project.title
      end
    end

    scenario 'must have new link' do
      expect(page).to have_link 'Novo'
    end

    scenario 'new link successfully redirects' do
      click_on 'Novo'
      expect(page).to have_content 'Novo Projeto'
    end

    scenario 'must have task link' do
      expect(page).to have_link 'Tarefas'
    end

    scenario 'edit link successfully redirects' do
      click_on 'Tarefas', match: :first
      expect(page).to have_content projects.last.title
    end

    scenario 'must have edit link' do
      expect(page).to have_link 'Editar'
    end

    scenario 'edit link successfully redirects' do
      click_on 'Editar', match: :first
      expect(page).to have_content 'Editar Projeto'
    end

    scenario 'must have delete link' do
      expect(page).to have_link 'Excluir'
    end
  end

  context 'new project' do
    before do
      visit new_project_path
    end

    scenario 'must have a link back' do
      expect(page).to have_link 'Voltar'
    end

    scenario 'link back works' do
      click_on 'Voltar'
      expect(page).to have_content 'Projetos'
    end

    scenario 'must have reset button' do
      expect(page).to have_button 'Resetar'
    end
  end

  context 'create project' do
    let(:project) { build(:project) }

    before do
      visit new_project_path
    end

    scenario 'with valid inputs' do
      within('form') do
        fill_in 'Título',  with: project.title
      end
      click_on 'Criar Projeto'
      expect(page).to have_content 'Projeto cadastrado com sucesso.'
    end

    scenario 'with invalid inputs' do
      click_on 'Criar Projeto'
      expect(page).to have_content '1 erro proibiu este projeto de ser salvo:'
      expect(page).to have_content 'Título não pode ficar em branco'
    end
  end

  context 'update project' do
    let!(:project) { create(:project, user: user) }

    before do
      visit edit_project_path(project)
    end

    scenario 'with valid inputs' do
      within('form') do
        fill_in 'Título',  with: 'Changed'
      end
      click_on 'Atualizar Projeto'
      expect(page).to have_content 'Projeto atualizado com sucesso.'
    end

    scenario 'with invalid inputs' do
      within('form') do
        fill_in 'Título',  with: nil
      end
      click_on 'Atualizar Projeto'
      expect(page).to have_content '1 erro proibiu este projeto de ser salvo:'
      expect(page).to have_content 'Título não pode ficar em branco'
    end
  end

  context 'destroy project', js: true do
    let!(:project) { create(:project, user: user) }

    scenario 'should be successful' do
      visit projects_path
      find(:xpath, '/html/body/section/div/div[2]/table/tbody/tr/td[3]/a[3]').click
      page.driver.browser.switch_to.alert.accept
      expect(page).to have_content 'Projeto excluído com sucesso.'
    end
  end
end
