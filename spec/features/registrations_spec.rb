require 'rails_helper'

RSpec.feature 'Registrations', type: :feature do
  let!(:user) { build(:user) }
  context 'user sign up' do
    before { visit new_user_registration_path }

    scenario 'when data is valid' do
      within('form') do
        fill_in 'Primeiro nome', with: user.first_name
        fill_in 'Último nome', with: user.last_name
        fill_in 'Nome de usuário', with: user.nickname
        fill_in 'E-mail', with: user.email
        fill_in 'Senha', with: user.password
        fill_in 'Confirmar senha', with: user.password
      end

      click_button 'Salvar'
      expect(page).to have_content 'Login efetuado com sucesso. Se não foi autorizado, a confirmação será enviada por e-mail.'
    end

    scenario 'when data is invalid' do
      within('form') do
        fill_in 'Primeiro nome', with: ''
        fill_in 'Último nome', with: ''
        fill_in 'Nome de usuário', with: ''
        fill_in 'E-mail', with: ''
        fill_in 'Senha', with: ''
        fill_in 'Confirmar senha', with: ''
      end

      click_button 'Salvar'
      expect(page).to have_content 'Não foi possível salvar usuário: 5 erros.'
      expect(page).to have_content 'E-mail não pode ficar em branco'
      expect(page).to have_content 'Senha não pode ficar em branco'
      expect(page).to have_content 'Primeiro nome não pode ficar em branco'
      expect(page).to have_content 'Último nome não pode ficar em branco'
      expect(page).to have_content 'Nome de usuário não pode ficar em branco'
    end
  end
end
