require 'rails_helper'

RSpec.feature 'Sessions', type: :feature do
  let!(:user) { create(:user, password: '123456') }

  context 'user sign in' do
    before { visit new_user_session_path }

    scenario 'when data is valid' do
      within('form') do
        fill_in 'Nome de usuário', with: user.nickname
        fill_in 'Senha', with: '123456'
      end

      click_button 'Entrar'
      expect(page).to have_content 'Login efetuado com sucesso!'
    end

    scenario 'when data is invalid' do
      within('form') do
        fill_in 'Nome de usuário', with: 'anyone'
        fill_in 'Senha', with: '654321'
      end

      click_button 'Entrar'
      expect(page).to have_content 'Nome de usuário ou senha inválida.'
    end
  end

  context 'user sign out' do
    before do
      login_as(user, scope: :user)
    end

    it 'click on link' do
      visit root_path
      click_link 'Sair'
      expect(page).to have_content 'Para continuar, efetue login ou registre-se.'
    end
  end
end
