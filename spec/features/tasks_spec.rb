require 'rails_helper'

RSpec.feature 'Tasks', type: :feature do
  let!(:user) { create(:user) }
  let!(:project) { create(:project, user: user) }

  before do
    login_as(user, scope: :user)
  end

  context 'task index' do
    let!(:tasks) { create_list(:task, 5, project: project) }

    before do
      visit project_tasks_path(project)
    end

    scenario 'must list tasks' do
      expect(page).to have_http_status(:ok)

      tasks.each do |task|
        expect(page).to have_content task.id
        expect(page).to have_content task.description
      end
    end

    scenario 'must have new link' do
      expect(page).to have_link 'Nova'
    end

    scenario 'new link successfully redirects' do
      click_on 'Nova'
      expect(page).to have_content 'Nova Tarefa'
    end

    scenario 'must have edit link' do
      expect(page).to have_link 'Editar'
    end

    scenario 'edit link successfully redirects' do
      click_on 'Editar', match: :first
      expect(page).to have_content 'Editar Tarefa'
    end

    scenario 'must have delete link' do
      expect(page).to have_link 'Excluir'
    end
  end

  context 'new task' do
    before do
      visit new_project_task_path(project)
    end

    scenario 'must have a link back' do
      expect(page).to have_link 'Voltar'
    end

    scenario 'link back works' do
      click_on 'Voltar'
      expect(page).to have_content 'Tarefas'
    end

    scenario 'must have reset button' do
      expect(page).to have_button 'Resetar'
    end
  end

  context 'create task' do
    let(:task) { build(:task) }

    before do
      visit new_project_task_path(project)
    end

    scenario 'with valid inputs' do
      within('form') do
        fill_in 'Descrição', with: task.description
        find(:xpath, "//*[@id='task_status']/option[2]").click
      end
      click_on 'Criar Tarefa'
      expect(page).to have_content 'Tarefa cadastrada com sucesso.'
    end

    scenario 'with invalid inputs' do
      click_on 'Criar Tarefa'
      expect(page).to have_content '1 erro proibiu esta tarefa de ser salva:'
      expect(page).to have_content 'Descrição não pode ficar em branco'
    end
  end

  context 'update task' do
    let!(:task) { create(:task, project: project) }

    before do
      visit edit_project_task_path(project, task)
    end

    scenario 'with valid inputs' do
      within('form') do
        fill_in 'Descrição', with: 'Changed'
      end
      click_on 'Atualizar Tarefa'
      expect(page).to have_content 'Tarefa atualizada com sucesso.'
    end

    scenario 'with invalid inputs' do
      within('form') do
        fill_in 'Descrição', with: nil
      end
      click_on 'Atualizar Tarefa'
      expect(page).to have_content '1 erro proibiu esta tarefa de ser salva:'
      expect(page).to have_content 'Descrição não pode ficar em branco'
    end
  end

  context 'destroy task', js: true do
    let!(:task) { create(:task, project: project) }

    scenario 'should be successful' do
      visit project_tasks_path(project)
      find(:xpath, '/html/body/section/div/div[2]/table/tbody/tr/td[4]/a[2]').click
      page.driver.browser.switch_to.alert.accept
      expect(page).to have_content 'Tarefa excluída com sucesso.'
    end
  end
end
